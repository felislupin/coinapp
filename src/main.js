
import Vue from 'vue'
import App from './App'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false

Vue.use(VueAxios, axios); // use to manage http requests.


new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
